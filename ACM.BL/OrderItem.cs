﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ACM.BL
{
   public class OrderItem
    {
        public OrderItem()
        {

        }
        public OrderItem(int orderItemId)
        {
            OrderItemId = orderItemId;
        }
        public int OrderItemId { get; private set; }
        public int ProductId { get; set; }
        public decimal? PurchasePrice { get; set; }
        public int Quantity { get; set; }

        //Retrieve one order item

        public OrderItem Retrieve(int orderItemId)
        {
            //code that retrieve the defined orderItem
            return new OrderItem();
        }

        public bool Save()
        {
            //code that saves the defined orderItem
            return true;
        }

        public bool Validate()
        {
            var isValid = true;

            if (Quantity <= 0) isValid = false;
            if (ProductId <= 0) isValid = false;
            if (PurchasePrice == null) isValid = false;

            return isValid;
        }
    }
}
