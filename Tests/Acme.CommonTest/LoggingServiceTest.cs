﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Acme.Common;
using ACM.BL;


namespace Acme.CommonTest
{
    /// <summary>
    /// Summary description for LoggingServiceTest
    /// </summary>
    [TestClass]
    public class LoggingServiceTest
    {
        

        [TestMethod]
        public void WriteToFile()
        {
            // Arrange
            var changedItems = new List<Iloggable>();

            var customer = new Customer(1)
            {
                EmailAddress = "fbaggins@hobbiton.me",
                FirstName = "Frodo",
                LastName = "Baggins",
                AddressList = null

            };

            changedItems.Add(customer);


            var product = new Product(2)
            {
                ProductName = "Rake",
                ProductDescription = "Garden Rake with Steel Head",
                CurrentPrice = 6M
            };
            changedItems.Add(product);

            // Act

            LoggingService.WriteToFile(changedItems);

            //Assert
            //Nothing to assert.
        }
    }
}
